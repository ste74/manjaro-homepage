---
#background: "img/bg4.jpg"
title: "Privacy Policy"

---
<h2>Who we are</h2>

<p>This Privacy Policy governs the manner in which <strong>Manjaro</strong> collects, uses, maintains and discloses information collected from users (each, a “User”) of the <a href="https://manjaro.org">website</a> will not send us any information, and statistical information will be stored on the user’s website.</p>

<h2>What personal data we collect and why we collect it</h2>

<h3>Personal identification information</h3>
<p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, place an order, fill out a form, respond to a survey, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>

<h3>Non-personal identification information</h3>
<p>We may collect non-personal identification information about Users whenever they interact with our Site. The non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Sites, such as the operating system and the Internet service providers utilized and other similar information.</p>

<h3>Contact forms</h3>
<p>Using our contact forms will use our own hosted mail server located in Germany. Your data will be only processed internally and not shared with any 3rd-party group in that case.</p>

<h3>Cookies</h3>
<p>Our Site may use “cookies” to enhance User experience. User’s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. The user may choose to set their web browser to refuse cookies or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>
<p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>
<p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p>
<p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &#8220;Remember Me&#8221;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p>
<p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p>

<h3>Embedded content from other websites</h3>
<p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p>
<p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.</p>

<h3>Media</h3>
<p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p>

<!--<h3>Analytics</h3>
<p>This website stores some user agent data. These data are used to provide a more personalized experience and to track your whereabouts around our website in compliance with the European General Data Protection Regulation. If you decide to opt-out of any future tracking, a cookie will be set up in your browser to remember this choice for one year. As analytics tool we use <a href="https://wp-statistics.com">WP Statistics</a> with unique hashes enabled. This prevents us from storing your IP address in our database. Visit their web page to learn more about their <a href="https://wp-statistics.com/privacy-and-policy/">Privacy Policy</a>.</p>
<p>To protect this webpage we use <a href="https://www.wordfence.com/">Wordfence</a> as security plugin which uses Google Analytics. Read their <a href="https://www.wordfence.com/privacy-policy/">Privacy Policy</a> if you want to know more. For more information regarding Google’s use of cookies and collection and use of information see the <a href="https://www.google.com/policies/privacy/">Google Privacy Policy</a>. To opt out of Google Analytics, please visit the <a href="https://support.google.com/analytics/answer/181881?hl=en">Google Analytics Opt-Out Page</a> to learn about opting out and installing the appropriate browser add-on.</p>-->

<h3>Payment Services</h3>
<p>We offer to donate to our project via <a href="https://www.paypal.com">PayPal</a>. Provider of this service is PayPal (Europe) S.à.r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg. When you chose to donate via PayPal, your entered payment data will be transferred to Paypal. Data transfer will be processed in compliance with the European General Data Protection Regulation. Data processing can be cancelled at any time. This cancellation however won&#8217;t be reflecting to already processed data transfers from the past. Learn more about <a href="https://www.paypal.com/us/webapps/mpp/ua/privacy-full">PayPal&#8217;s Privacy Policy</a> by visiting their web page.</p>

<h2>How we use collected information</h2>
<p>Our website may collect and use Users personal information for the following purposes:<br />
– To run and operate our Site<br />
We may need your information display content on the Site correctly.<br />
– To improve customer service<br />
The information you provide helps us respond to your customer service requests and support needs more efficiently.<br />
– To personalize user experience<br />
We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.<br />
– To improve our Site<br />
We may use feedback you provide to improve our products and services.<br />
– To run a promotion, contest, survey or other Site feature<br />
To send Users information they agreed to receive about topics we think will be of interest to them.<br />
– To send periodic emails<br />
We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests.</p>

<h2>How we protect your information</h2>
<p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>

<h2>Sharing your personal information</h2>
<p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners and trusted affiliates for the purposes outlined above. We may use third-party service providers to help us operate our business and the Site or administer activities on our behalfs, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p>

<h2>How long we retain your data</h2>
<p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p>
<p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p>

<h2>What rights you have over your data</h2>
<p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p>

<h2>Where we send your data</h2>
<p>Visitor comments may be checked through an automated spam detection service.</p>

<h2>Changes to this privacy policy</h2>
<p>Our website has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site, revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

<h2>Your acceptance of these terms</h2>
<p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>

<h2>Contacting us</h2>
<p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us. Our contact information you may get from our <a href="/terms-of-use/">Terms of Use</a> page.</p>
