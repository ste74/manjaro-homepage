+++
Download_x64 = "https://osdn.net/dl/manjaro-community/manjaro-awesome-18.0-stable-x86_64.iso"
Download_x64_Checksum = "004032021f803f6ed463dd8704bee30b8dfde39b"
Download_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-awesome-18.0-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-awesome-18.0-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Awesome Edition"
Screenshot = "awesome-full.jpg"
ShortDescription = "This edition is supported by the Manjaro community and comes with Awesome, a highly configurable, next generation framework window manager for X."
Tags = [ "resourceefficient" ]
TargetGroup = "For power users and developers dealing with every day computing tasks"
Thumbnail = "awesome.jpg"
Version = "18.0"
date = "2018-11-08T08:02:46:12+01:00"
title = "Awesome - Stable"
type="download-edition"
weight = "0"
+++

