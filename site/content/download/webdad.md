+++
Download_x64 = "https://osdn.net/dl/manjaro-community/manjaro-webdad-18.0-alpha-4-unstable-x86_64.iso"
Download_x64_Checksum = "50f9d1c1144f5047ab338f7ce47f9779d648b617"
Download_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-webdad-18.0-alpha-4-unstable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-webdad-18.0-alpha-4-unstable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Webdad Edition (Developer Preview)"
Screenshot = "webdad-full.jpg"
ShortDescription = "This edition is supported by the Manjaro community and comes with Just Another Desktop Environment (JADE) made with Web technologies."
Tags = [ "preview" ]
TargetGroup = "For people loving the web and thinking out of the box"
Thumbnail = "webdad-preview.jpg"
Version = "18.0-alpha-4"
date = "2018-05-10T08:56:12+02:00"
title = "Webdad - Developer Preview"
type="download-edition"
weight = "0"
+++

**Note: This is a preview version for developers and testers and is not suitable for production environment as it may contain bugs**

Jade is a completely different DE concept, that changes the way you interact with your desktop, is made to be easy to use, independently of your computer skills.

Native notifications to speech functionality. Desktop/ browser or phone notifications, are read by JADE voice to you, so you wont have to stop what you are doing to read your notifications, Text notifications are still supported.

KDE Connect is used to communicate across all your devices. You can receive your phone notifications on your desktop computer, control music playing on your phone from your desktop, or use your phone as a remote control for your desktop.

KDE Connect for Android can be found in the Google Play Store and F-droid.
This is a work in progress.
