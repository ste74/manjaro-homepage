+++
Download_x64 = "https://osdn.net/dl/manjaro-community/manjaro-openbox-18.0-stable-x86_64.iso"
Download_x64_Checksum = "72a72c01e80fcbba2961e272d324bd4324cc7d02"
Download_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-openbox-18.0-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-openbox-18.0-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "openbox Edition"
Screenshot = "openbox-full.jpg"
ShortDescription = "This edition is supported by the Manjaro community and comes with openbox, a highly configurable, next generation window manager with extensive standards support."
Tags = [ "resourceefficient" ]
TargetGroup = "For power users and developers dealing with every day computing tasks"
Thumbnail = "openbox.jpg"
Version = "18.0"
date = "2018-11-05T00:49:12+01:00"
title = "openbox - Stable"
type="download-edition"
weight = "0"
+++

