+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-gnome-18.0-stable-x86_64.iso"
Download_x64_Checksum = "e7d600adbf3bbc29d671239193f9589232c9fe99"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-gnome-18.0-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-gnome-18.0-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "GNOME Edition"
Screenshot = "gnome-full.jpg"
ShortDescription = "This edition is supported by the Manjaro community and comes with the GNOME 3 desktop that breaks with traditional concepts and allows users to focus on their tasks. Desktop-specific applications are crafted with care and clearly defined by guidelines that make them more consistent to use."
Tags = [ "official", "beginnerfriendly" ]
TargetGroup = "For people who want a very modern and simple desktop"
Thumbnail = "gnome.jpg"
Version = "18.0"
date = "2018-10-29T21:36:00+02:00"
title = "Gnome - Stable"
type="download-edition"
weigth = "3"
+++

