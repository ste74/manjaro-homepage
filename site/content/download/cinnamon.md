+++
Download_x64 = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-18.0-stable-x86_64.iso"
Download_x64_Checksum = "fd68c59b5207d36b4a1b0d90265ac718fffc3d44"
Download_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-18.0-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-cinnamon-18.0-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Cinnamon Edition"
Screenshot = "cinnamon-full.jpg"
ShortDescription = "This edition is supported by the Manjaro community and comes with Cinnamon, a desktop based on modern technology that keeps known and proven concepts."
Tags = [ "resourceefficient", "beginnerfriendly", "traditional_ui" ]
TargetGroup = "For people who look for a traditional desktop with modern technology"
Thumbnail = "cinnamon.jpg"
Version = "18.0"
date = "2018-11-29T17:46:12+01:00"
title = "Cinnamon - Stable"
type="download-edition"
weight = 0
+++

