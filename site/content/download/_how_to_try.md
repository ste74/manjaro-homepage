+++
type="download-how-to-try"
+++

<!--- ## Try it now
--- --->
You can try Manjaro <strong>without modifying your current operating system,</strong> run Manjaro from a USB stick or a DVD. Later on if you decide to install Manjaro, you can do it directly from the live system.
<div class="row">
 <ul class="list-group card col-md-12 offset-xl-1 col-xl-10">
 <li class="list-group-item text-center"><h2>Ways you can try Manjaro</h2></li>
    <li class="list-group-item">
   <img src="/img/try/virtual-machine.svg" class="icon">Using a [virtual machine](/support/firststeps#using-a-virtual-machine) within your current operating system
   </li>
<li class="list-group-item"> 
 <img src="/img/try/live-boot.svg" class="icon">By [running it](/support/firststeps#making-a-live-system) from a DVD or USB Stick
  </li>
 <li class="list-group-item">
  <img src="/img/try/virtual-machine.svg" class="icon">[Install](/support/firststeps#install-manjaro) from a DVD or USB Stick
 </li>
</ul>
</div>
