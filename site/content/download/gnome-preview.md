+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-gnome-18.0.1-rc2-stable-x86_64.iso"
Download_x64_Checksum = "039fa875fe202353109e2fe9bdbad7f9420bfc30"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-gnome-18.0.1-rc2-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-gnome-18.0.1-rc2-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "GNOME Edition (Developer Preview)"
Screenshot = "gnome-preview.jpg"
ShortDescription = "This edition is supported by the Manjaro community and comes with the GNOME 3 desktop that breaks with traditional concepts and allows users to focus on their tasks. Desktop-specific applications are crafted with care and clearly defined by guidelines that make them more consistent to use. This preview is not suitable for production environment."
Tags = [ "official", "preview" ]
TargetGroup = "For people who want a very modern and simple desktop"
Thumbnail = "gnome-preview.jpg"
Version = "18.0.1-rc1"
date = "2018-11-26T21:36:00+02:00"
title = "Gnome - Development Preview"
type="download-edition"
weigth = "0"
+++

**Note: This is a preview version for developers and testers and is not suitable for production environment as it may contain bugs**
