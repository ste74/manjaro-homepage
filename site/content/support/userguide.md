+++
date = "2016-09-05T21:03:22+02:00"
title = "User Guide"
type = "user-guide"
weight = 0
right = true
navsection = "support"
+++

<p class="text-center">
  <a href="https://de.osdn.net/projects/manjaro/storage/Manjaro-User-Guide.pdf" class="btn btn-success btn-xl" >Download User Guide in English</a>
  <a href="https://de.osdn.net/projects/manjaro/storage/Manjaro-User-Guide-French.pdf" class="btn btn-success btn-xl" >Download User Guide in French</a>
<p>
<div class="row">
  <p class="col-md-10 offset-md-1 offset-xl-2 col-xl-8">Our team and our community proudly present you the Manjaro User Guide, a over 100 page long book with everything new and advanced users can know about Manjaro. We cover following topics in our guide:
  </p>
</div>
<div class="row">
  <ul class="col-md-6 col-xl-4 list-group">
    <img class="xl-icon ml-auto mr-auto" src="/img/actions/download.svg">
    <h2>Preparation</h2>
    <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Getting Manjaro</li>
    <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Downloading Manjaro</li>
    <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Check downloaded iso image for errors</li>
    <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Writing iso image</li> 
 </ul>
<div class="col-md-6 col-xl-4 list-group">
    <img class="xl-icon ml-auto mr-auto" src="/img/actions/install.svg">
    <h2>Installation</h2>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Installing Manjaro</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Booting the Live environment</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Assisted installation on a BIOS system with Calamares</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Manual installation on a BIOS system with Calamares</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Assisted installation on a UEFI system with Calamares</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Manual installation on a UEFI system with Calamares</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Dual-booting with Microsoft Windows 10</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Advanced installation methods</li>
</div>
<div class="col-md-6 col-xl-4 list-group">
    <img class="xl-icon ml-auto mr-auto" src="/img/try/install.svg">
    <h2>Using Manjaro</h2>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Welcome to Manjaro</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>The Manjaro desktop</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Getting help</li>
   <li class="list-group-item"><img src="/img/actions/information.svg" alt="Icon" class="sm-icon"/>Maintaining your system</li>
</div>
</div>