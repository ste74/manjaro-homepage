{
  "title" : "Try it now",
  "type" : "download-selector",
  "filters" : [
  { "id" : "official", "description" : "Flagship editions", "default" : "checked" },
  { "id" : "beginnerfriendly", "description" : "Beginner-friendly", "default" : "checked" },
  { "id" : "resourceefficient", "description" : "Resource-efficient", "default" : "" },
  { "id" : "traditional_ui", "description" : "Traditional workflow", "default" : "" }
  ]
}

For simplicity we have selected XFCE, KDE and GNOME as our flagship editions. If you want to look for other experiences, our community has created many flavours that focus around other desktops.

For example we have editions that use technology that makes them very resource-efficient and makes them a good choice for *old computers*. Other editions focus on breaking away from user interface standards that are decades old and giving users a modern experience. As not everybody wants to have such an experience and likes a more traditional way how everything works, we have editions that focus on this *traditional workflow*.

Simply use our download selector below to find your right edition. *You may also want to checkout our archive of past released [flagship](https://osdn.net/projects/manjaro/storage) and [community](https://osdn.net/projects/manjaro-community/storage) editions.*
