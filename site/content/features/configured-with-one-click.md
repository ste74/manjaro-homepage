+++
date = "2016-09-06T00:36:21+02:00"
navsection = "features"
right = false
title = "Configured With One Click"
type = "features-post"
weight = 1
+++

<div class="row">
<p class="ml-auto mr-auto col-md-8">
Manjaro comes with it's own tools that help you configuring your computer the way you want - without searching for scattered settings across the whole system and looking things up on the internet. Just open the system control panel, open the tool and select what you want.
</p>
</div>

<div class="row">
<div class="col-md-3 offset-md-1"><img class="zoom" src="/img/applications/manjaro_pamac.png"/></div>
<section id="pamac" class="col-md-7">
  <div class="feature-screenshot-fan">
    <div>
    <h3>Choose, install, finished!</h3>
    <p>Manjaro maintains it's own program to install, update and remove packages from the system. Just open it, choose what you want and click 'install'. All other needed things will be done automatically and the only thing you need to do is to start the program.</p>
    </div>
  </div>
</section>


<div class="col-md-3 offset-md-1"><img class="zoom" src="/img/applications/manjaro_locale.png"/></div>
<section id="locale" class="col-md-7">
  <div class="feature-screenshot-fan">
    <div >
    <h3>Manjaro speaks your language</h3>
    <p>If your native language isn't English, it's no problem for Manjaro. Just start our locale settings and select the language you want. After you logged out and in again, everything is exactly the way you want. We also offer you a special tool that automatically installs language packs for a variety of programs and notifies you if you can get translation sfor your favorite programs.
    </p>
    </div>
    
  </div>
</section>


<div class="col-md-3 offset-md-1"><img class="zoom" src="/img/applications/manjaro_mhwd.png"/></div>
<section id="mhwd" class="col-md-7">
  <div class="feature-screenshot-fan">
    <div>
    <h3>New graphics card? One click and it's done</h3>
    <p>Manjaro comes with MHWD, the Manjaro Hardware Detection. If you need drivers for your hardware, just open it, click on "Auto Install" and wait until it's finished. That's it.</p>
    </div>
  </div>
</section>

<div class="col-md-3 offset-md-1"><img class="zoom" src="/img/applications/manjaro_timeanddate.png"/></div>
<section id="timeanddate" class="col-md-7">
  <div class="feature-screenshot-fan">
    <div>
    <h3>Traveling a lot?</h3>
    <p>If you need to change the timezone, because you are on holiday or you need to travel for your business, we provide you a handy tool to quickly switch the time settings to have more time to get things done or enjoy the free time.</p>
    </div>
  </div>
</section>

<div class="col-md-3 offset-md-1"><img class="zoom" src="/img/applications/manjaro_useraccounts.png"/></div>
<section id="useraccounts" class="col-md-7">
  <div class="feature-screenshot-fan">
    <div>
    <h3>Divide and conquer</h3>
    <p>If multiple people use your computer, just create an account for each of them. So everybody can have their own data and own privileges what they can and can't do.</p>
    </div>
  </div>
</section>

<div class="col-md-3 offset-md-1"><img class="zoom" src="/img/applications/manjaro_kernels.png"/></div>
<section id="kernel" class="col-md-7">
  <div class="feature-screenshot-fan">
    <div>
    <h3>Switch the core with one click</h3>
    <p>Power-users and people who need special features of the Linux Kernel that don't come with the default one can just switch to a different Kernel with one click. Do you make music or need other realtime features? Just switch to a RT-Kernel. Do you need the newest improved open-source graphics drivers? Just switch to a newer one. Manjaro let's you not only switch to another Kernel, but supports multiple installed kernels at the same time. Just re-boot your system and you can just switch between all installed Kernels in the boot menu.
    </p>
    </div>
  </div>
</section>
</div>
